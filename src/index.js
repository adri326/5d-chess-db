const { spawn, Pool, Worker } = require('threads');
const fs = require('fs');
const os = require('os');
var cpuCount = os.cpus().length - 1;
cpuCount = cpuCount <= 0 ? 1 : cpuCount;

try { fs.mkdirSync('./db'); } catch(err){}
try { fs.mkdirSync('./db/standard'); } catch(err){}
try { fs.mkdirSync('./db/defended-pawn'); } catch(err){}
try { fs.mkdirSync('./db/half-reflected'); } catch(err){}
try { fs.mkdirSync('./db/princess'); } catch(err){}
try { fs.mkdirSync('./db/turn-zero'); } catch(err){}

var run = async () => {
  const pool = Pool(() => spawn(new Worker('./gen')));
  for(var i = 0;i < cpuCount;i++) {
    pool.queue(async gen => {
      while(true) {
        console.log(Date.now() + ': Job queued');
        try {
          var data = await gen.gen();
          try { 
            fs.mkdirSync(data.path);
          }
          catch(err){}
          try { 
            fs.writeFileSync(data.path + '/' + data.filename, data.data); 
          }
          catch(err){
            console.error(err);
          }
          console.log(Date.now() + ': Writing data to ' + data.path + '/' + data.filename);
        }
        catch(err) {
          console.error(err);
        }
      }
    });
  }
  await pool.completed();
};

run().catch(console.error);